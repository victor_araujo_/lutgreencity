#Fork of the boilerplate from
http://przeor.github.io/react-router-flux-starter-kit/#/

#Dependencies
	node, npm
	gulp (npm install -g gulp)
	httpster (npm install -g httpster)

#Installation
From "reactJS-flux-starter-kit" directory run

	npm install
	then run "gulp" - this will create new dist/ directory
	use *gulp watch* for live reload

#Running
from reactJS-flux-starter-kit/dist/ (created by gulp) directory run
    
	httpster

use gulp for building once or gulp watch for automatic updates
	go to http://localhost:3333		

#Locations:
	Parse keys: src/index.html
	Facebook keys: src/js/utils/fboauth.js
