var React = require('react');
var Router = require('react-router');
var Route = Router.Route;
var RouteHandler = Router.RouteHandler;
var Link = Router.Link;


APP = require('./components/app').APP;

var Logout = require('./components/auth/app-logout');
var Login = require('./components/auth/app-login');
var About = require('./components/about/app-about');
var Dashboard = require('./components/dashboard/app-dashboard');
var Cover = require('./components/cover/cover');
var Datagen = require('./components/datagen/datagen');
var Tips = require('./components/tips/tips');
var Register = require('./components/auth/app-register');

var routes = (
  <Route handler={APP}>
    <Route name="login" handler={Login}/>
    <Route name="logout" handler={Logout}/>
    <Route name="about" handler={Cover}/>
    <Route name="dashboard" handler={Dashboard}/>
    <Route name="cover" handler={Cover}/>
    <Route name="datagen" handler={Datagen}/>
    <Route name="tips" handler={Tips}/>
    <Route name="register" handler={Register}/>

  </Route>
);

Router.run(routes, function (Handler) {
  React.render(<Handler/>, document.getElementById('app'));
});
