var AppDispatcher = require('../dispatchers/app-dispatcher');
var AppConstants = require('../constants/app-constants');
var EventEmitter = require('events').EventEmitter;
var React = require('react/addons');

var CHANGE_EVENT = "change";

var _dataPushed = [];

function _changeData(data) {
  _dataPushed = data;
}

var AppStore = React.addons.update(EventEmitter.prototype, { $merge: {
  emitChange:function(){
    this.emit(CHANGE_EVENT);
  },

  addChangeListener:function(callback){
    this.on(CHANGE_EVENT, callback)
  },
  removeChangeListener:function(callback){
    this.removeListener(CHANGE_EVENT, callback)
  },
  
  getDataPushed: function() {
    return _dataPushed;
  },

  dispatcherIndex:AppDispatcher.register(function(payload){
    var action = payload.action; // this is our action from handleViewAction
    switch(action.actionType){
      case AppConstants.POST_READING:
        _changeData(payload.action.data);
        break;
    }
    AppStore.emitChange();

    return true;
  })
}});

module.exports = AppStore;