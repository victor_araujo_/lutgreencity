var AppDispatcher = require('../dispatchers/app-dispatcher');
var AppConstants = require('../constants/app-constants');
var EventEmitter = require('events').EventEmitter;
var React = require('react/addons');
var Api = require('../utils/api');

var CHANGE_EVENT = "change";

var _cartItems = [];

var _topics = [];

var _apiData = [];

var _tips = [];

var _randomTip = [];

var _userData = {};

var _ranking = {
        statistics: {
          average: 0,
          ratio: 0
        }
      };

function _addUserData(data) {
  _userData = data;
}

function _addItem(item){
  _cartItems.push(Math.random());
}

function _sendApi(data) {
  _apiData.push(data)
}

function _getTopics(topics) {
  _topics = topics;
}

function search(criteria) {
}

function _getTips(tips) {
  _tips = tips;
}

function _deleteTip(key) {
  delete _tips[key];
}

function _addTip(id, data) {
  _tips[id] = data;
}

function _getRanking(data) {
  _ranking = data;
}

function _getRandomTip(tip) {
  _randomTip = tip;
}

var AppStore = React.addons.update(EventEmitter.prototype, { $merge: {
  emitChange:function(){
    this.emit(CHANGE_EVENT);
  },

  addChangeListener:function(callback){
    this.on(CHANGE_EVENT, callback)
  },
  removeChangeListener:function(callback){
    this.removeListener(CHANGE_EVENT, callback)
  },
  getCart:function(){
    return _cartItems
  },
  getState:function() {
    return  {topics: this.topics};
  },
  getTopics:function() {
    return _topics;
  },
  getData: function() {
    return _apiData;
  },
  getScheduleList: function() {
    return [];
  },
  print: function() {
  },
  getTips: function() {
    return _tips;
  },
  getUserData: function() {
    return _userData;
  },
  getRanking: function() {
    return _ranking;
  },
  getTip: function() {
    return _randomTip;
  },
  dispatcherIndex:AppDispatcher.register(function(payload){
    var action = payload.action; // this is our action from handleViewAction
    switch(action.actionType){
      case AppConstants.ADD_ITEM:
        _addItem(payload.action.item);
        break;

      case AppConstants.GET_FB_USER_DATA:
        _addUserData(payload.action.data);
        break;

      case AppConstants.SEND_API:
        _sendApi(payload.action.data);
        break;

      case AppConstants.SEARCH:
        _search(payload.action.criteria);
        break;

      case AppConstants.GET_TOPICS:
        _getTopics(payload.action.topics);
        break;

      case AppConstants.GET_TIPS:
        console.log(payload.action.tips);
        _getTips(payload.action.tips);
        break;

      case AppConstants.POST_TIP:
        _addTip(payload.action.id, payload.action.data);
        break;

      case AppConstants.DELETE_TIP:
        _deleteTip(payload.action.id);
        break;    

      case AppConstants.GET_RANKING:
        _getRanking(payload.action.data);     

      case AppConstants.GET_RANDOM_TIP:
        _getRandomTip(payload.action.tip);
    }
    AppStore.emitChange();

    return true;
  })
}});

module.exports = AppStore;