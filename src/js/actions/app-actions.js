/** @jsx React.DOM */
var AppConstants = require('../constants/app-constants.js');
var AppDispatcher = require('../dispatchers/app-dispatcher.js');
var Api = require('../utils/api');
var sha1 = require('sha1');


var AppActions = {
  addItem:function(item){
    AppDispatcher.handleViewAction({
      actionType: AppConstants.ADD_ITEM,
      item: item
    })
  },
  sendApi:function(data){
    AppDispatcher.handleViewAction({
      actionType: AppConstants.SEND_API,
      data: data
    })
  },
  search:function(criteria){
    AppDispatcher.handleViewAction({
      actionType: AppConstants.SEARCH,
      criteria: criteria
    })
  },
  getEntityData: function(entityId) {
    Api.getEntityData(entityId);
  },
  getRanking: function(userId) {
    Api.get("http://lutgreencityapi.herokuapp.com/v1/ranking/"+sha1(userId))
    .then(function(json) {
      json = JSON.parse(json);
      AppDispatcher.handleViewAction({
       actionType: AppConstants.GET_RANKING,
       data: json
      });
    });
  },
  getFirebaseUserData: function(userId) {
  	Api.get("http://lutgreencityapi.herokuapp.com/v1/users/"+sha1(userId))
    .then(function(json) {
      AppDispatcher.handleViewAction({
	     actionType: AppConstants.GET_FB_USER_DATA,
       data: json
      });
    });
  },
  postFirebaseUserData: function(userId, data) {
  	Api.post("https://lutgreencity.firebaseio.com/"+sha1(userId), data)
  	AppDispatcher.handleViewAction({
	   actionType: AppConstants.POST_FB_USER_DATA,
	   });
  },
  getTips: function() {
    Api.get("http://lutgreencityapi.herokuapp.com/v1/tips/")
    .then(function(json) {
      data = [];
      for (var obj in json) {
        var temp = [];
        json.id = obj;
        temp[obj] = json[obj];
        //data.push(temp);
        //data.push({obj: json[obj]});
        data[obj] = json[obj];
      }
      AppDispatcher.handleViewAction({
        actionType: AppConstants.GET_TIPS,
        tips: data
      });
    });
  },  
  getRandomTip: function() {
    Api.get("http://lutgreencityapi.herokuapp.com/v1/tips/random")
    .then(function(json) {
      data = [];
      for (var obj in json) {
        var temp = [];
        json.id = obj;
        temp[obj] = json[obj];
        //data.push(temp);
        //data.push({obj: json[obj]});
        data[obj] = json[obj];
      }
      console.log(data);
      AppDispatcher.handleViewAction({
        actionType: AppConstants.GET_RANDOM_TIP,
        tip: data
      });
    });
  },  
  postTip: function(userId, data) {
    js = {createdBy: userId, tip: data, tags: "energy, saving, home, general"};
    Api.post("http://lutgreencityapi.herokuapp.com/v1/tips/", js)
    .then(function(json) {
      return json.json();
    }).then(function(json) {
      AppDispatcher.handleViewAction({
        actionType: AppConstants.POST_TIP,
        id: JSON.parse(json).name,
        data: js
      });
    });
  },
  deleteTip: function(key) {
    Api.del("http://lutgreencityapi.herokuapp.com/v1/tips/"+key)
    .then(function(json) {
      AppDispatcher.handleViewAction({
        actionType: AppConstants.DELETE_TIP,
        id: key
      });
    });
  },
  postReading: function(userId, data) {
    json_obj = {userid: sha1(userId), date: data.time.date, data: data};
    Api.post("http://lutgreencityapi.herokuapp.com/v1/energy_readings/", json_obj)
    .then(function(json) {
      return json.json();
    }).then(function(json) {
      AppDispatcher.handleViewAction({
        actionType: AppConstants.POST_READING,
        data: data
      });
    });
  },
  getTopics: function() {
  	Api.get("https://api.imgur.com/3/topics/defaults")
      .then(function(json) {
        AppDispatcher.handleViewAction({
  	    actionType: AppConstants.GET_TOPICS,
  	    topics: json.data
  	  })
    });
  },
  register: function(information) {
    Api.post("http://lutgreencityapi.herokuapp.com/v1/createUser/", information)
    .then(function(json) {
      var payload = {
          'actionType': AppConstants.AUTH_LOG_IN, 
          'email': information.email, 
          'pass': information.pass
        };
        AppDispatcher.handleViewAction(payload);
    }).then(function(json) {
        

    });
  }
}

module.exports = AppActions;