/*
 * Copyright (c) 2014-2015, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * listActions
 */

var AppDispatcher = require('../dispatchers/AppDispatcher');
var listConstants = require('../constants/listConstants');

var listActions = {

  /**
   * @param  {string} text
   */
  create: function(text) {
    AppDispatcher.dispatch({
      actionType: listConstants.list_CREATE,
      text: text
    });
  },

  /**
   * @param  {string} id The ID of the list item
   * @param  {string} text
   */
  updateText: function(id, text) {
    AppDispatcher.dispatch({
      actionType: listConstants.list_UPDATE_TEXT,
      id: id,
      text: text
    });
  },

  /**
   * Toggle whether a single list is complete
   * @param  {object} list
   */
  toggleComplete: function(list) {
    var id = list.id;
    var actionType = list.complete ?
        listConstants.list_UNDO_COMPLETE :
        listConstants.list_COMPLETE;

    AppDispatcher.dispatch({
      actionType: actionType,
      id: id
    });
  },

  /**
   * Mark all lists as complete
   */
  toggleCompleteAll: function() {
    AppDispatcher.dispatch({
      actionType: listConstants.list_TOGGLE_COMPLETE_ALL
    });
  },

  /**
   * @param  {string} id
   */
  destroy: function(id) {
    AppDispatcher.dispatch({
      actionType: listConstants.list_DESTROY,
      id: id
    });
  },

  /**
   * Delete all the completed lists
   */
  destroyCompleted: function() {
    AppDispatcher.dispatch({
      actionType: listConstants.list_DESTROY_COMPLETED
    });
  }

};

module.exports = ListActions;