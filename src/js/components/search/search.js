var React = require('react');
var Login = require('../auth/app-login');
var AuthStore = require('../../stores/app-auth.js');
var AppActions = require('../../actions/app-actions.js')


var Search = React.createClass({
  getInitialState: function() {
    return {criteria: []};
  },
  render: function() {
    return <div>
      {this.renderList()}
    </div>
  },
  handleClick: function() {
    AppActions.search(this.state.criteria);
  },
  handleTextChange: function(e) {
    this.setState({text: e.target.value});
    console.log(this.state.text);
  }, 
  renderList: function() {
     var ListItems = this.state.List.map(function(item,i){
        return <tr key={i}><td>{item.status}</td><td>{item.fullname}</td><td>{item.email}</td></tr>
      });
      return (
        <div>
          <div></div>
          <input type="text" onChange={this.handleTextChange} value={this.text}/>
          <button onClick={this.handleClick}>Go</button>
          <table className="table table-striped table-hover">
          <thead>
            <tr><th>Status</th><th>Fullname</th><th>Email</th></tr>
          </thead>
          <tbody>
          {ListItems}
          </tbody>
          </table>
        </div>
      );
    }
  
});

module.exports = Search;

