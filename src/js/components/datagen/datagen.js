var React = require('react');
var AppActions = require('../../actions/app-actions.js')
var AuthStore = require('../../stores/app-auth.js');
var AuthenticationMixin = require('../../mixins/AuthenticationMixin.js');
var moment = require('moment');
var DatePicker = require('../datepicker/datepicker.js');

var Datagen = React.createClass({
  dataModel :          {
            "device" : {
              "company" : {
                "brand" : "Aeon Labs",
                "capabilities" : {
                  "air_conditioning" : false,
                  "heating" : false,
                  "measure_energy" : true,
                  "measure_humidity" : false,
                  "measure_temperature" : false
                },
                "device_id" : "peyiJNo0IldT2YlIVtYaGQ",
                "locale" : "fi-FI",
                "model" : "AEDSB09104ZWUS Aeotec",
                "name" : "Main circuit meter",
                "software_version" : "3.2.1",
                "type" : "ENERGY METER"
              },
              "country_code" : "FI",
              "postal_code" : "00120",
              "reading" : {
                "energy" : "2",
                "energy_grid" : {
                  "currency" : "EUR",
                  "peak_period_end_time" : "2016-10-31T23:59:59.000Z",
                  "peak_period_start_time" : "2016-10-31T23:59:59.000Z",
                  "price_per_kWh" : 0.013
                },
                "energy_unit" : "kWh",
                "humidity" : 0,
                "temperature" : 0,
                "temperature_unit" : "C"
              },
              "time_zone" : "Finland/Helsinki"
            },
            "metadata" : {
              "access_token" : "35vj7e9wswqqs7bsxlxr",
              "client_version" : 1
            },
            "time" : {
              "date" : "2016-4-17",
              "day" : 17,
              "month" : 4,
              "timestamp" : 1460910718744,
              "year" : 2016
            }
          },

  mixins: [ AuthenticationMixin ],

  getInitialState: function() {
    var currentDate = new Date();
    return {
      time: {
        date: currentDate.getFullYear()+"-"+(currentDate.getMonth()+1)+"-"+currentDate.getDate(),
        year: currentDate.getFullYear(),
        month: (currentDate.getMonth()+1),
        day: currentDate.getDate(),
        timestamp: currentDate.getTime()
      }, 
      metadata: {
        access_token: AuthStore.authGetToken(),
        client_version: 1
      },
      device: {
        company: {
          type: "ENERGY METER",
          brand: "Aeon Labs",
          model: "AEDSB09104ZWUS Aeotec",
          device_id: "peyiJNo0IldT2YlIVtYaGQ",
          software_version: "3.2.1",
          locale: "fi-FI",
          name: "Main circuit meter",
          capabilities: {
            measure_energy: true,
            measure_temperature: false,
            measure_humidity: false,
            air_conditioning: false,
            heating: false
          }
        },
        country_code: "FI",
        postal_code: "53850",
        time_zone: "Finland/Helsinki",
        reading: {
          energy: 0,
          energy_grid: {
            price_per_kWh: 0.130,
            carbon_factor: 0.225457295,
            currency: "EUR",
            peak_period_start_time: "2016-10-31T23:59:59.000Z",
            peak_period_end_time: "2016-10-31T23:59:59.000Z"
          },
          energy_unit: "kWh",
          temperature: 0,
          temperature_unit: "C",
          humidity: 0
        }
      },  
    };
  },

  componentWillMount: function() {
    //AppStore.addChangeListener(this.updateData);
  },
  handleChange: function(date) {
    this.setState({
      time: {
        date: date.getFullYear()+"-"+(date.getMonth()+1)+"-"+date.getDate(),
        year: date.getFullYear(),
        month: (date.getMonth()+1),
        day: date.getDate(),
        timestamp: date.getTime()
      }
    }); 
    console.log(this.state);
  },
  handleEnergyChange: function(e) {
    var device = React.addons.update(this.state.device, {
      reading: {
        energy: {$set: e.target.value}
      }
    });
    
    this.setState({
      device: device
    }, function() {console.log(this.state);});
    
  },
  handleButtonClick: function() {
    AppActions.postReading(AuthStore.getEmail(), this.state);
  },

  render: function() {
    return ( 
      <div className="form-group">
        <label for="date">Date</label>
        <DatePicker selected={""} onSelect={this.handleChange} />
        <label for="energy">Energy reading for the day</label>
        <br/>
        <input type="number" onChange={this.handleEnergyChange}/>
        <div> 
          <button className="btn btn-primary" onClick={this.handleButtonClick}>Send data</button>
        </div>
        <code>
          <pre>
            {JSON.stringify(this.dataModel, null, 2)}
          </pre>      
        </code>

      </div>
      )
  }
  
});

module.exports = Datagen;


