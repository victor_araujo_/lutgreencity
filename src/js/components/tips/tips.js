var React = require('react');
var AppActions = require('../../actions/app-actions.js');
var AuthStore = require('../../stores/app-auth.js');
var AppStore = require('../../stores/app-store.js');
var Tip = require('./tip.js');
var AuthenticationMixin = require('../../mixins/AuthenticationMixin.js');


var Tips = React.createClass({
  mixins: [ AuthenticationMixin ],

  getInitialState: function() {
    return {
      text: "",
      tips: []
    };
  },
  updateData: function() {
    this.setState({tips: AppStore.getTips()});
    console.log(AppStore.getTips());
  },
  handleTextChange: function(e) {
    this.setState({
      text: e.target.value
    });
    //this.updateData();
  }, 
 handleClick: function() {
    AppActions.postTip(AuthStore.getEmail(), this.state.text);
    //AppActions.getTips();
  },
  componentWillMount: function() {
    AppStore.addChangeListener(this.updateData);
    AppActions.getTips();
  },
  renderTips: function() {
    for(var key in this.state.tips) {
      var item = this.state.tips[key];
      children.push(
        <Tip key={key} item={item} id={key}>
        </Tip>
      );
    };
    return children;
  },
  render: function() {
    console.log(this.state.tips);

    var children = [];
    for (var key in this.state.tips) {
      children.push( <Tip key={key} item={this.state.tips[key]} id={key}>
        </Tip> ); 
    }

    return (
      <div>
        <h2>Tips</h2>
        <input type="text" onChange={this.handleTextChange} value={this.text}/>
        <button onClick={this.handleClick}>Go</button>
        {children}
      </div>
    );
  },
  
});

module.exports = Tips;


