var React = require('react');
var AppActions = require('../../actions/app-actions.js');

module.exports = React.createClass({
  getInitialState: function() {    
    return {
      tip: this.props.item.tip,
      key: this.props.id,
      textChanged: false
    }
  },
  componentWillMount: function() {
  },
  render: function() {
    return <div className="input-group">
      <input type="text"
        className="form form-control"
        value={this.state.tip}
        onChange={this.handleTextChange}
        />
      <span className="input-group-btn">
        {this.changesButtons()}
        <button
          className="btn btn-primary"
          onClick={this.handleDeleteClick}
          >
          Delete
        </button>
      </span>
    </div>
  },
  changesButtons: function() {
    return null;
    /*
    if(!this.state.textChanged) {
      return null
    } else {
      return [
        <button
          className="btn btn-default"
          onClick={this.handleSaveClick}
          >
          Save
        </button>,
        <button
          onClick={this.handleUndoClick}
          className="btn btn-default"
          >
          Undo
        </button>
      ]
    } */
  },
  handleSaveClick: function() {
    //this.fb.update({text: this.state.text});
    this.setState({textChanged: false});
  },
  handleUndoClick: function() {
    this.setState({
      text: this.props.item.text,
      textChanged: false
    });
  },
  handleTextChange: function(event) {
    this.setState({
      text: event.target.value,
      textChanged: true
    });
  },
  handleDoneChange: function(event) {
    var update = {done: event.target.checked}
    this.setState(update);
    //this.fb.update(update);
  },
  handleDeleteClick: function() {
    AppActions.deleteTip(this.state.key);
    AppActions.getTips();
  }
});