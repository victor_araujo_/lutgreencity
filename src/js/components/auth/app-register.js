/** @jsx React.DOM */
var React = require('react');
var Router = require('react-router');
var AppStore = require('../../stores/app-store.js');
var AppActions = require('../../actions/app-actions.js');
var AuthAction = require('../../actions/app-auth.js');
var AuthStore = require('../../stores/app-auth.js');



var Register = React.createClass({
  contextTypes: {
    router: React.PropTypes.func.isRequired
  },

  getInitialState: function () {
    return {
      email: "",
      age: "",
      type: "",
      latitude: 0,
      longitude: 0
    }
  },
  componentDidMount: function() {
  },
  componentDidUpdate: function() {
   
  },
  componentWillMount: function() {
    AuthStore.addChangeListener(this._onAuth);
  },
  componentWillUnmount: function() {
  },
  handleSubmit: function (event) {
    event.preventDefault();
    var information = {};
    information.email = this.refs.email.getDOMNode().value;
    information.username = this.refs.username.getDOMNode().value;
    information.materialType = this.refs.type.getDOMNode().value;
    information.heating = this.refs.heating.getDOMNode().value;
    information.number_people = this.refs.number_people.getDOMNode().value;
    information.pass = "password1";
    information.location = {};
    information.location.latitude = this.refs.latitude.getDOMNode().value;
    information.location.longitude = this.refs.longitude.getDOMNode().value;
    console.log(information);
    AppActions.register(information);
    AuthAction.startAuth(information.email, information.pass);
    


  },
  _onAuth: function() {this.context.router.replaceWith('/dashboard');},
  render: function () {
    var errors = this.state.login_error === true ? <p>Bad login information</p> : '';
    return (
      <div>
      <div>
        <h3>Register</h3>
        <p>Register to create a profile for our service and to participate in a more sustainable world!</p>
        <hr/>
      </div>

      <div className="form form-group">
        <form onSubmit={this.handleSubmit}>

          <label>Email</label><input className="form-control" ref="email" placeholder="email" />
          <label>Username</label><input className="form-control" ref="username" placeholder="username"/>
          <label>Material</label><input className="form-control" ref="type" placeholder="type" defaultValue="Bricks and mortar"/>
          <label>Heating</label><input className="form-control" ref="heating" placeholder="heating" defaultValue="Heat pump"/> 
          <label>Number of people in dwelling</label><input type="number" className="form-control" ref="number_people" placeholder="number_people" defaultValue="1"/>
          <label>Latitude</label><input className="form-control" type="number" ref="latitude" placeholder="latitude" defaultValue="61.0554106"/>
          <label>Longitude</label><input className="form-control" type="number" ref="longitude" placeholder="longitude" defaultValue="28.0670232"/>

          <button className="btn btn-primary" type="submit">Register</button>
          {errors}
        </form>
       </div>
       </div>
    );
  }
});

module.exports = Register;