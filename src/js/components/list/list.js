var React = require('react');
var Login = require('../auth/app-login');
var AuthStore = require('../../stores/app-auth.js');
var AppStore = require('../../stores/app-store.js');
var AppActions = require('../../actions/app-actions.js')
var Firebase = require('firebase');
var ReactFire = require('reactfire');

function getData() {
  return {List: DashboardStore.getScheduleList()}
};

var List = React.createClass({
  mixins: [ 
  ReactFire ],
  onSendApi: function() {
    console.log("onsendchange");
    this.setState({
      List: AppStore.getScheduleList(),
      ApiData: AppStore.getData()
    });
  },
  handleDataLoaded: function() {
    for(var key in this.firebaseRefs.items) {
        var item = this.firebaseRefs.items[key];
        item.key = key;
        console.log(item);
        console.log(key);
    }
  },
  getInitialState: function() {
    return {
      topics: [],
      List: [], 
      text: "",
      ApiData: []
    };
  },
  componentWillMount: function() {
    AppStore.addChangeListener(this._onChange);
  },
  componentDidMount: function() {
    AppActions.getFirebaseUserData(AuthStore.getEmail());
    //console.log(this.state.fbdata);
  },
  _onChange: function() {
    this.setState({
      topics: AppStore.getTopics()
    });
  },
  render: function() {
    return <div>
      {this.renderList()}
    </div>
  },
  updateData: function() {
    this.setState({
      List: AppStore.getScheduleList(),
      ApiData: AppStore.getData()
    });
  },
  handleClick: function() {
    AppActions.sendApi(this.state.text);
    this.updateData();
  },
  handleTextChange: function(e) {
    this.setState({
      text: e.target.value
    });
    this.updateData();
    //console.log(this.state.text);
  }, 

  renderTopics: function() {
    return this.state.topics.map(function(topic) {
      return <li>
        {topic}
      </li>
    })
  },
  renderList: function() {
     var ListItems = this.state.List.map(function(item,i){
        return <tr key={i}><td>{item.status}</td><td>{item.fullname}</td><td>{item.email}</td></tr>
      });
      var PushedData = this.state.ApiData.map(function(item,i){
        return <li>{item}</li>
      });
      return (
        <div>
          <div></div>
          <input type="text" onChange={this.handleTextChange} value={this.text}/>
          <button onClick={this.handleClick}>Go</button>
          <table className="table table-striped table-hover">
          <thead>
            <tr><th>Status</th><th>Fullname</th><th>Email</th></tr>
          </thead>
          <tbody>
          {ListItems}
          <ul>
          {PushedData}
          </ul>
          <ul>
            {this.renderTopics()}
          </ul>
          </tbody>
          </table>
        </div>
      );
  }
  
});

module.exports = List;

