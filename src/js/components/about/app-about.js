/** @jsx React.DOM */
var React = require('react');
var Login = require('../auth/app-login');
var AuthStore = require('../../stores/app-auth.js');
var AuthenticationMixin = require('../../mixins/AuthenticationMixin.js');


var About = React.createClass({
  mixins: [ AuthenticationMixin ],
  render: function () {
    return (
    	<div>
    		<h1>About</h1>
    		<p>Description of the project and the technologies.
    		<br/>
    		This react router flux starter has been prepared with all known best practises
    		about react flux architecture.
    		<br/>
    		It extends the react-router-flux-starter-kit from <a href="https://github.com/przeor/">PrzeoR</a> and uses:
    		<ul>
    			<li>FB flux architecture</li>
                <li>React JS</li>
    			<li>basic react-router with auth flow</li>
    		</ul>
    		</p>
    	</div>
    	);
  }
});

module.exports = About;
