/** @jsx React.DOM */
var React = require('react');
var Login = require('../auth/app-login');
var AuthStore = require('../../stores/app-auth.js');
var ScheduleList = require('./app-schedulelist');
var List = require('../list/list.js');
var AuthenticationMixin = require('../../mixins/AuthenticationMixin.js');
var AppActions = require('../../actions/app-actions.js');
var AppStore = require('../../stores/app-store.js');
var LineChart = require("react-chartjs").Line;
var Chart = require('chart.js'); 


var Dashboard = React.createClass({
  mixins: [ AuthenticationMixin ],

  getInitialState: function() {
    return {
      username: "Loading",
      energy_consumption: "...",
      location: {
        latitude: 61.047878,
        longitude: 28.11139 
      },
      ranking: {
        statistics: {
          average: 0,
          ratio: 0
        }
      },
      randomTip: {
        tip: "Loading..."
      },
      chartData: {
          labels: ["January", "February", "March", "April", "May", "June", "July"],
          datasets: [
              {
                  label: "My First dataset",
                  fillColor: "rgba(220,220,220,0.2)",
                  strokeColor: "rgba(220,220,220,1)",
                  pointColor: "rgba(220,220,220,1)",
                  pointStrokeColor: "#fff",
                  pointHighlightFill: "#fff",
                  pointHighlightStroke: "rgba(220,220,220,1)",
                  data: [65, 59, 80, 81, 56, 55, 40]
              },
              {
                  label: "My Second dataset",
                  fillColor: "rgba(151,187,205,0.2)",
                  strokeColor: "rgba(151,187,205,1)",
                  pointColor: "rgba(151,187,205,1)",
                  pointStrokeColor: "#fff",
                  pointHighlightFill: "#fff",
                  pointHighlightStroke: "rgba(151,187,205,1)",
                  data: [28, 48, 40, 19, 86, 27, 90]
              }
          ]
      
      }
    };
  },

  onChange: function(e) {
    this.setState({text: e.target.value});
  },

  onGetData: function() {
    var newState = AppStore.getUserData();
    this.setState(newState);
    var newRanking = AppStore.getRanking();
    this.setState({ranking: newRanking});
    var randomTip = AppStore.getTip();
    this.setState({randomTip: randomTip});
  },

  componentWillMount: function() {
    AppStore.addChangeListener(this.onGetData);
    AppActions.getFirebaseUserData(AuthStore.getEmail());
    AppActions.getRanking(AuthStore.getEmail());
    AppActions.getRandomTip();
  },

  componentDidMount: function() {
    var ctx = document.getElementById("myChart").getContext("2d");
    var myNewChart = new Chart(ctx).Line(this.state.data);
  },

  handleSubmit: function(e) {
  },

  createMap: function() {
    return {
      __html: '<iframe width="500" height="450" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q='+this.state.location.latitude+'%2C%20'+this.state.location.longitude+'&zoom=12&center='+this.state.location.latitude+'%2C%20'+this.state.location.longitude+'&key=AIzaSyAKuywwZV90x6q75mTYdvJEgEz1pw9wI6U" allowfullscreen></iframe>"'
    }
  },

  render: function () {
    var token = AuthStore.authGetToken();

    var ListItems = [];
    for (user in this.state.ranking.users) {
      ListItems.push(<li>{this.state.ranking.users[user].username}: {this.state.ranking.users[user].energy_consumption} kWh</li>);
    };

    return (
      <div>
        <h1>Dashboard</h1>

        <table className="table table-striped table-hover">
          <thead>
            <tr><th>Username</th><th>Total Energy Consumption</th><th></th></tr>
          </thead>
          <tbody>
            <tr>
              <td>{this.state.username}</td>
              <td>{this.state.energy_consumption} kWh</td>
            </tr>
          </tbody>
          </table>

        <div className="row">
        </div> 


        <div className="row">
          <div className="col-md-6" dangerouslySetInnerHTML={ this.createMap() } />
          <div className="col-md-6">
            <div className="row">
              <div className="panel panel-default">
              <div className="panel panel-heading panel-primary">Your consumption compared to others</div>
                <ul>
                  <li>You have consumed {this.state.energy_consumption} kWh</li>
                  <li>In terms of least energy consumption per person, you are ranked {this.state.ranking.ranking} / {this.state.ranking.count} in your area.</li>
                  <li>The average energy consumption in your area is {this.state.ranking.statistics.average.toFixed(2)} kWh. Your consumption is {this.state.ranking.statistics.ratio.toFixed(2)} times the average.</li>
                  <li>The current rankings among your friends are:
                    <ol>
                      {ListItems}
                    </ol>
                  </li> 
                </ul>  
              </div>
              </div>
            <div/>

            <div className="row">
              <div className="panel panel-default">
              <div className="panel panel-heading panel-primary">Tips to reduce your energy consumption</div>
                <ul>
                  <li>{this.state.randomTip.tip}</li>
                </ul>
              </div>
              </div>
            <div/>  
          </div>
        </div> 

        
      </div>
    );
  }
});

module.exports = Dashboard;
