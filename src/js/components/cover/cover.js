/** @jsx React.DOM
* Cover template
* Background images from href="http://join.deathtothestockphoto.com/">Death to the Stock Photo<
 */
var React = require('react');
var Router = require('react-router');
var Route = Router.Route;
var RouteHandler = Router.RouteHandler;
var auth = require('../../stores/app-auth'); // TODO / USE DISPATCHER & ACTIONS
var Login = require('../auth/app-login');
var AppActions = require('../../actions/app-actions.js');
var AuthStore = require('../../stores/app-auth.js');
var AppStore = require('../../stores/app-store.js');
var Link = Router.Link;
var Api = require('../../utils/api.js');

var Cover = React.createClass({
    getInitialState: function() {
        return {topics: []};
    },
    componentWillMount: function() {
        AppStore.addChangeListener(this._onChange);
    },
    componentDidMount: function() {
        //AppActions.getTopics();
    },
    _onChange: function() {
        console.log("change");
        this.setState({
            topics: AppStore.getTopics()
        });
    },
    renderTopics: function() {
        console.log(this.state);
        return this.state.topics.map(function(topic) {
            return <li>
                {topic}
                </li>
        })
    },
  render: function () {
    return (
    	<div>
		<header id="top" className="header">
        <div className="text-vertical-center">
            <h1>Eco Estate</h1>
            <h3>Green your house, save money</h3>
            <br/>
            <ul>
                <img id="logoImg" src="../../assets/img/logo.png" />
            </ul>
        </div>
    </header>

    <div className="center">
                        <img id="logoImg2" className="center" src="../../assets/img/logo.png" />

    </div>


    <section id="about" className="about">
        <div className="container">
            <div className="row">
                <div className="col-lg-12 text-center">
                    <h2>Engaging homeowners and technology!</h2>
                    <p className="lead">We analyze the consumption data from your smart devices and provide tips and personalized analytics to help you save energy. By using our product you can save money and contribute to improve data collection for sustainability.</p>
                </div>
            </div>
        </div>
    </section>

    <section id="services" className="services bg-primary">
        <div className="container">
            <div className="row text-center">
                <div className="col-lg-10 col-lg-offset-1">
                    <h2>Our Services</h2>
                    <hr className="small" />
                    <div className="row">
                        <div className="col-md-3 col-sm-6">
                            <div className="service-item">
                                <span className="fa-stack fa-4x">
                                <i className="fa fa-circle fa-stack-2x"></i>
                                <i className="fa fa-cloud fa-stack-1x text-primary"></i>
                            </span>
                                <h4>
                                    <strong>Cloud storage of your home devices data</strong>
                                </h4>
                                <p>Historical data from IoT devices are stored and can be accessed through an API</p>
                            </div>
                        </div>
                        <div className="col-md-3 col-sm-6">
                            <div className="service-item">
                                <span className="fa-stack fa-4x">
                                <i className="fa fa-circle fa-stack-2x"></i>
                                <i className="fa fa-compass fa-stack-1x text-primary"></i>
                            </span>
                                <h4>
                                    <strong>Analytics</strong>
                                </h4>
                                <p>Learn about the meaning of the recollected data and get tips to improve the efficiency of your home!</p>
                            </div>
                        </div>
                        <div className="col-md-3 col-sm-6">
                            <div className="service-item">
                                <span className="fa-stack fa-4x">
                                <i className="fa fa-circle fa-stack-2x"></i>
                                <i className="fa fa-flask fa-stack-1x text-primary"></i>
                            </span>
                                <h4>
                                    <strong>Formulas for success</strong>
                                </h4>
                                <p>Get the best tips for home efficiency.</p>
                            </div>
                        </div>
                        <div className="col-md-3 col-sm-6">
                            <div className="service-item">
                                <span className="fa-stack fa-4x">
                                <i className="fa fa-circle fa-stack-2x"></i>
                                <i className="fa fa-shield fa-stack-1x text-primary"></i>
                            </span>
                                <h4>
                                    <strong>Defend against costs</strong>
                                </h4>
                                <p>Our service has the potential for savings.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


   


    <footer>

 <aside className="call-to-action bg-primary">
        <div className="container">
            <div className="row">
                <div className="col-lg-12 text-center">
                    <h3>Get started now!</h3>
                    <a href="#/register" className="btn btn-lg btn-light">Register</a>
                    <a href="#/login" className="btn btn-lg btn-dark">Have an account? Log in</a>
                </div>
            </div>
        </div>
    </aside>

        <div className="container">
            <div className="row">
                <div className="col-lg-10 col-lg-offset-1 text-center">
                    <h4><strong>Software and Systems as a Service</strong>
                    </h4>
                    <p>Lappeenranta University of Technology<br />FI 53850</p>
                    <ul className="list-unstyled">
                        <li><i className="fa fa-phone fa-fw"></i> (123) 456-7890</li>
                        <li><i className="fa fa-envelope-o fa-fw"></i>  <a href="mailto:ve.ar91@gmail.com">ve.ar91@gmail.com</a>
                        </li>
                    </ul>
                    <br />
                    <ul className="list-inline">
                        <li>
                            <a href="#"><i className="fa fa-facebook fa-fw fa-3x"></i></a>
                        </li>
                        <li>
                            <a href="#"><i className="fa fa-twitter fa-fw fa-3x"></i></a>
                        </li>
                        <li>
                            <a href="#"><i className="fa fa-dribbble fa-fw fa-3x"></i></a>
                        </li>
                    </ul>
                    <hr className="small" />
                    <p className="text-muted">Copyright &copy; 2016</p>
                </div>
            </div>
        </div>
    </footer>
			</div>
    );
  }
});

module.exports = Cover;
