var AppConstants = require('../constants/app-constants');
var AppDispatcher = require('../dispatchers/app-dispatcher');
var request = require('superagent');

var Fetch = require('whatwg-fetch');
var rootUrl = 'https://api.imgur.com/3/';
var apiKey = '3a09849e13a0466';
var clientID = '3a09849e13a0466';
var clientSecret = '94bfd5ea9c4ab6188836d48cc7f41f3b3b68238a';

// 
// TODO - structure AppConstants for API as for example: http://www.code-experience.com/async-requests-with-react-js-and-flux-revisited/
// 
// console.log("********** utils/api.js init");


var API_URL = '/assets/api.json';
var TIMEOUT = 10000;

var _pendingRequests = {};



function abortPendingRequests(key) {
    if (_pendingRequests[key]) {
        _pendingRequests[key]._callback = function(){};
        _pendingRequests[key].abort();
        _pendingRequests[key] = null;
    }
}

function token() {
    return "test"; // TODO authentication with using AuthStore.getState().token;
}

function makeUrl(part) {
    return API_URL + part;
}

function dispatch(key, response, params) {
    var payload = {actionType: key, response: response};
    if (params) {
        payload.queryParams = params;
    }
    AppDispatcher.handleRequestAction(payload);
}

// return successful response, else return request Constants
function makeDigestFun(key, params) {
    return function (err, res) {
        if (err && err.timeout === TIMEOUT) {
            dispatch(key, AppConstants.TIMEOUT, params);
        } else if (res.status === 400) {
            UserActions.logout();
        } else if (!res.ok) {
            dispatch(key, AppConstants.ERROR, params);
        } else {
            dispatch(key, res, params);
        }
    };
}

// a get request with an authtoken param
function get(url) {
    return request
        .get(url)
        .timeout(TIMEOUT)
        .query({authtoken: token()});
}

var Api = {
    getEntityData: function(entityId) {
        var url = makeUrl("?test="+entityId);
        var key = AppConstants.GET_ENTITY_DATA;
        var params = {entityId: entityId};
        abortPendingRequests(key);
        dispatch(key, AppConstants.PENDING, params);
        _pendingRequests[key] = get(url).end(
            makeDigestFun(key, params)
        );
    },
    getFB: function(url) {
        return fetch(url, {
        }).then(function(response){
            return response.json();
        }).then(function(response) {
            return response;
        });
    },
    get: function(url) {
        return fetch(url, {
            headers: {
                'Authorization': 'Client-ID ' + apiKey
            }
        }).then(function(response){
            return response.json();
        }).then(function(response) {
            return response;
        });
    },

    post: function(url, data) {
        return fetch(url, {
          method: 'POST',
          mode: 'cors', 
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(data)
        })
    },

    del: function(url) {
        return fetch(url, {
          method: 'DELETE',
        })
    } 
};

module.exports = Api;


